**Para visualizar el mapa dinámico haga clic [aquí](https://jfvalbuenal.gitlab.io/semaforosgc/)**

# Semáforo de avance: Clasificación de unidades hidrogeológicas SGC

El visualizador reúne los avances que por equipos se vienen desarrollando en la 
tarea por clasificar de menara inicial un mapa hidrogeológico para Colombia, en esta
fase se usan como geometría base los polígono que conforman el mapa cronoestratigráfico de Colombia escala 1:1M año 2015.

El visor es construido sobre `R 4.1.1`

A su vez se han requerido funcionalidades específicas de las librerías:

 - leafem_0.1.6
- spatialEco_1.3-7 
- htmlwidgets_1.5.3
- htmltools_0.5.2
- maptools_1.1-1
- rgeos_0.5-5
- here_1.0.1
- raster_3.4-13
- rgdal_1.5-23
- dplyr_1.0.7      
- leaflet_2.0.4.1
- sf_1.0-2         
- sp_1.4-5 

  ## Contenido
El visualizador está compuesto de 3 módulos, un script principal y dos auxiliares, la ejecución de realiza desde el módulo  `VisorMapWeb.R` y su resultado se guarda como archivo `html` en la carpeta raíz de trabajo. La información temática agrupada se ha centrado en la presentación de *Grupos hidrogeológicos*, *Sistemas acuíferos* y *niveles de confianza*.

Cada una de estas visualizaciones contiene comentarios explicativos por polígonos, desplegables al realizar clic sobre las unidades.     

